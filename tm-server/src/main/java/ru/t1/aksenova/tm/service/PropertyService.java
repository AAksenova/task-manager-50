package ru.t1.aksenova.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Cleanup;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.aksenova.tm.api.service.IPropertyService;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import static java.lang.ClassLoader.getSystemResourceAsStream;

@Service
public final class PropertyService implements IPropertyService {

    @NotNull
    public static final String APPLICATION_FILE_NAME_KEY = "config";

    @NotNull
    public static final String APPLICATION_FILE_NAME_DEFAULT = "application.properties";

    @NotNull
    public static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    public static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    public static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    public static final String PASSWORD_ITERATION_DEFAULT = "52631";

    @NotNull
    public static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    public static final String PASSWORD_SECRET_DEFAULT = "659814653";

    @NotNull
    public static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    public static final String SERVER_PORT_DEFAULT = "8080";

    @NotNull
    public static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    public static final String SERVER_HOST_DEFAULT = "localhost";

    @NotNull
    public static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    public static final String SERVER_SESSION_KEY_DEFAULT = "222333666";

    @NotNull
    public static final String SERVER_SESSION_KEY = "session.key";

    @NotNull
    public static final String SERVER_SESSION_TIMEOUT_DEFAULT = "1000";

    @NotNull
    public static final String SERVER_SESSION_TIMEOUT_KEY = "session.timeout";

    @NotNull
    public static final String DATABASE_USERNAME_DEFAULT = "postgres";

    @NotNull
    public static final String DATABASE_USERNAME_KEY = "database.username";

    @NotNull
    public static final String DATABASE_USER_PASSWORD_DEFAULT = "admin";

    @NotNull
    public static final String DATABASE_USER_PASSWORD_KEY = "database.password";

    @NotNull
    public static final String DATABASE_DRIVER_DEFAULT = "=org.postgresql.Driver";

    @NotNull
    public static final String DATABASE_DRIVER_KEY = "database.driver";

    @NotNull
    public static final String DATABASE_URL_DEFAULT = "jdbc:postgresql://localhost:5432/tm";

    @NotNull
    public static final String DATABASE_URL_KEY = "database.url";

    @NotNull
    public static final String DATABASE_DIALECT = "database.dialect";

    @NotNull
    public static final String DATABASE_HBM2DDL_AUTO = "database.hbm2ddl_auto";

    @NotNull
    public static final String DATABASE_SHOW_SQL = "database.show_sql";

    @NotNull
    public static final String DATABASE_FORMAT_SQL = "database.format_sql";

    @NotNull
    public static final String DB_CACHE_USE_SECOND_LVL_KEY = "database.cache.use_second_level_cache";

    @NotNull
    public static final String DB_CACHE_USE_MINIMAL_PUTS_KEY = "database.cache.use_minimal_puts";

    @NotNull
    public static final String DB_CACHE_USE_QUERY_CACHE_KEY = "database.cache.use_query_cache";

    @NotNull
    public static final String DB_CACHE_REGION_PREFIX_KEY = "database.cache.region_prefix";

    @NotNull
    public static final String DB_CACHE_PROVIDER_CONFIG_FILE_PATH_KEY = "database.cache.provider_configuration_file_resource_path";

    @NotNull
    public static final String DB_CACHE_FACTORY_CLASS_KEY = "database.cache.region.factory_class";

    @NotNull
    public static final String DATABASE_TOKEN_INIT_KEY = "database.init.token";

    @NotNull
    public static final String DATABASE_TOKEN_INIT_DEFAULT = "123123";

    @NotNull
    public static final String LIQUIBASE_PROPERTY_FILE_NAME_KEY = "liquibase.config.filename";

    @NotNull
    public static final String LIQUIBASE_PROPERTY_FILE_NAME_DEFAULT = "changelog/changelog-master.xml";

    @NotNull
    public static final String EMPTY_VALUE = "---";

    @NotNull
    public static final String GIT_BRANCH = "gitBranch";

    @NotNull
    public static final String GIT_COMMIT_ID = "gitCommitId";

    @NotNull
    public static final String GIT_COMMIT_TIME = "gitCommitTime";

    @NotNull
    public static final String GIT_COMMIT_MESSAGE = "gitCommitMessage";

    @NotNull
    public static final String GIT_COMMITTER_NAME = "gitCommitterName";

    @NotNull
    public static final String GIT_COMMITTER_NAME1 = "gitCommitterName";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        final boolean existConfig = isExistsExternalConfig();
        if (existConfig) loadExternalConfig(properties);
        else loadInternalConfig(properties);
    }

    @SneakyThrows
    private void loadExternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        @Cleanup @NotNull final InputStream inputStream = new FileInputStream(file);
        properties.load(inputStream);
    }

    @SneakyThrows
    private void loadInternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = APPLICATION_FILE_NAME_DEFAULT;
        @Cleanup @Nullable final InputStream inputStream = getSystemResourceAsStream(name);
        if (inputStream == null) return;
        properties.load(inputStream);
    }

    private boolean isExistsExternalConfig() {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        return file.exists();
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return "1.39.0";
        // return read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getApplicationConfig() {
        return getStringValue(APPLICATION_FILE_NAME_KEY, APPLICATION_FILE_NAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return "Anastasiya Aksenova";
        //return read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return "aaksenova@t1-consulting.ru";
        // return read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public String getGitBranch() {
        return read(GIT_BRANCH);
    }

    @NotNull
    @Override
    public String getGitCommitId() {
        return read(GIT_COMMIT_ID);
    }

    @NotNull
    @Override
    public String getGitCommitTime() {
        return read(GIT_COMMIT_TIME);
    }

    @NotNull
    @Override
    public String getGitCommitMessage() {
        return read(GIT_COMMIT_MESSAGE);
    }

    @NotNull
    @Override
    public String getGitCommitterName() {
        return read(GIT_COMMITTER_NAME);
    }

    @NotNull
    @Override
    public String getGitCommitterEmail() {
        return read(GIT_COMMITTER_NAME1);
    }

    @NotNull
    private String read(@Nullable final String key) {
        if (key == null || key.isEmpty()) return EMPTY_VALUE;
        if (!Manifests.exists(key)) return EMPTY_VALUE;
        return Manifests.read(key);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    public Integer getIntegerValue(
            @NotNull final String key,
            @NotNull final String defaultValue
    ) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    @NotNull
    public String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    public String getStringValue(
            @NotNull final String key,
            @NotNull final String defaultValue
    ) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKay = getEnvKey(key);
        if (System.getenv().containsKey(envKay)) return System.getenv(envKay);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getIntegerValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        return getIntegerValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
    }

    @NotNull
    @Override
    public String getServerHost() {
        return getStringValue(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @NotNull
    @Override
    public String getSessionKey() {
        return getStringValue(SERVER_SESSION_KEY, SERVER_SESSION_KEY_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getSessionTimeout() {
        return getIntegerValue(SERVER_SESSION_TIMEOUT_KEY, SERVER_SESSION_TIMEOUT_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseConnection() {
        return getStringValue(DATABASE_URL_KEY, DATABASE_URL_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseUser() {
        return getStringValue(DATABASE_USERNAME_KEY, DATABASE_USERNAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabasePassword() {
        return getStringValue(DATABASE_USER_PASSWORD_KEY, DATABASE_USER_PASSWORD_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseDriver() {
        return getStringValue(DATABASE_DRIVER_KEY, DATABASE_DRIVER_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseHibernateDialect() {
        return getStringValue(DATABASE_DIALECT, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseHibernateHbm2ddl() {
        return getStringValue(DATABASE_HBM2DDL_AUTO, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseHibernateShowSql() {
        return getStringValue(DATABASE_SHOW_SQL, "false");
    }

    @NotNull
    @Override
    public String getDatabaseHibernateFormatSql() {
        return getStringValue(DATABASE_FORMAT_SQL, "false");
    }

    @NotNull
    @Override
    public String getDatabaseCacheUseSecondLevelCache() {
        return getStringValue(DB_CACHE_USE_SECOND_LVL_KEY, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseCacheUseMinimalPuts() {
        return getStringValue(DB_CACHE_USE_MINIMAL_PUTS_KEY, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseCacheUseQueryCache() {
        return getStringValue(DB_CACHE_USE_QUERY_CACHE_KEY, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseCacheRegionPrefix() {
        return getStringValue(DB_CACHE_REGION_PREFIX_KEY, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseCacheProviderConfigFileResourcePath() {
        return getStringValue(DB_CACHE_PROVIDER_CONFIG_FILE_PATH_KEY, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseCacheRegionFactoryClass() {
        return getStringValue(DB_CACHE_FACTORY_CLASS_KEY, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDatabaseInitToken() {
        return getStringValue(DATABASE_TOKEN_INIT_KEY, DATABASE_TOKEN_INIT_DEFAULT);
    }

    @NotNull
    @Override
    public String getLiquibasePropertiesFilename() {
        return getStringValue(LIQUIBASE_PROPERTY_FILE_NAME_KEY, LIQUIBASE_PROPERTY_FILE_NAME_DEFAULT);
    }

}
