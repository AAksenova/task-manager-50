package ru.t1.aksenova.tm.service.model;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.aksenova.tm.api.repository.model.IProjectRepository;
import ru.t1.aksenova.tm.api.service.model.IProjectService;
import ru.t1.aksenova.tm.enumerated.ProjectSort;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.aksenova.tm.exception.field.*;
import ru.t1.aksenova.tm.model.Project;
import ru.t1.aksenova.tm.model.User;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
@AllArgsConstructor
public class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    @NotNull
    @Override
    public IProjectRepository getRepository() {
        return context.getBean(IProjectRepository.class);
    }

    @NotNull
    @Override
    public Project create(@Nullable final Project project) {
        if (project == null) throw new ProjectNotFoundException();
        add(project);
        return project;
    }

    @NotNull
    @Override
    public Project create(
            @Nullable final String userId,
            @Nullable final Project project
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (project == null) throw new ProjectIdEmptyException();
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        project.setUser(entityManager.find(User.class, userId));
        add(project);
        return project;
    }

    @NotNull
    @Override
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull Project project = new Project();
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        project.setUser(entityManager.find(User.class, userId));
        project.setName(name);
        project.setDescription(description);
        add(project);
        return project;
    }

    @Override
    @NotNull
    public Collection<Project> set(@NotNull Collection<Project> projects) {
        if (projects.isEmpty()) return Collections.emptyList();
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            projects.forEach(repository::add);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.findAll(userId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId, @Nullable final ProjectSort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        return findAll(userId, sort.getComparator());
    }

    @NotNull
    @Override
    public List<Project> findAll(
            @Nullable final String userId,
            @Nullable final Comparator comparator
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll(userId);
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @NotNull final List<Project> projects = repository.findAll(userId, comparator);
            if (projects.isEmpty() || projects == null) return Collections.emptyList();
            return projects;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public Project findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            @Nullable final Project project = repository.findOneById(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            return project;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            repository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public Project removeOne(
            @Nullable final String userId,
            @Nullable final Project project
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (project == null) throw new ProjectNotFoundException();
        remove(userId, project);
        return project;
    }

    @Nullable
    @Override
    public Project removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        remove(userId, project);
        return project;
    }


    @NotNull
    @Override
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        update(project);
        return project;
    }

    @NotNull
    @Override
    public Project changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusIncorrectException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        update(project);
        return project;
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return (repository.findOneById(userId, id) != null);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public long getSize(@Nullable final String userId) {
        @NotNull final IProjectRepository repository = getRepository();
        @NotNull final EntityManager entityManager = repository.getEntityManager();
        try {
            return repository.getCount(userId);
        } finally {
            entityManager.close();
        }
    }

}
