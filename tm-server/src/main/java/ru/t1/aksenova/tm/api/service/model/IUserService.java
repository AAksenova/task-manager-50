package ru.t1.aksenova.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.repository.model.IUserRepository;
import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.model.User;

import java.util.Collection;
import java.util.List;

public interface IUserService extends IService<User> {

    @NotNull
    IUserRepository getRepository();

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password
    );

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    );

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password,
            @Nullable Role role
    );

    @NotNull Collection<User> set(@NotNull Collection<User> users);

    @NotNull
    List<User> findAll();

    @Nullable
    User findOneById(@Nullable String id);

    @NotNull
    User findByLogin(@Nullable String login);

    @NotNull
    User findByEmail(@Nullable String email);

    @Nullable
    User removeOneById(@Nullable String id);

    void clear();

    @Nullable
    User removeOne(@Nullable User user);

    @Nullable
    User removeOneByLogin(@Nullable String login);

    @Nullable
    User removeOneByEmail(@Nullable String email);

    @NotNull
    User setPassword(
            @Nullable String id,
            @Nullable String password
    );

    @NotNull
    User updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    boolean isLoginExist(@Nullable String login);

    boolean isEmailExist(@Nullable String email);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(@Nullable String login);

}
