package ru.t1.aksenova.tm.component;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.aksenova.tm.api.endpoint.*;
import ru.t1.aksenova.tm.api.repository.ICommandRepository;
import ru.t1.aksenova.tm.api.service.*;
import ru.t1.aksenova.tm.command.AbstractCommand;
import ru.t1.aksenova.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.aksenova.tm.exception.system.CommandNotSupportedException;
import ru.t1.aksenova.tm.util.SystemUtil;
import ru.t1.aksenova.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.aksenova.tm.command";

    @Nullable
    @Autowired
    private AbstractCommand[] abstractCommands;

    @NotNull
    @Autowired
    private ICommandRepository commandRepository;

    @Getter
    @NotNull
    @Autowired
    private ICommandService commandService;

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @Getter
    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Getter
    @NotNull
    @Autowired
    private ITokenService tokenService;

    @Getter
    @NotNull
    @Autowired
    private ISystemEndpoint systemEndpointClient;

    @Getter
    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpointClient;

    @Getter
    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpointClient;

    @Getter
    @NotNull
    @Autowired
    private IAuthEndpoint authEndpointClient;

    @Getter
    @NotNull
    @Autowired
    private IUserEndpoint userEndpointClient;

    @Getter
    @NotNull
    @Autowired
    private IAdminEndpoint adminEndpointClient;


    private void initCommand(@NotNull final AbstractCommand[] commands) {
        for (@Nullable final AbstractCommand command : commands) {
            if (command == null) return;
            commandService.add(command);
        }
    }

    public void start(final String[] args) {
        if (processArguments(args)) System.exit(0);
        prepareStartup();
        initCommand(abstractCommands);
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private boolean processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        processArgument(arg);
        return true;
    }

    private void processArgument(@Nullable final String argument) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    public void processCommand(@Nullable final String command) {
        processCommand(command, true);
    }

    public void processCommand(@Nullable final String command, boolean checkRoles) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    private void prepareStartup() {
        loggerService.info("** WELCOME TO CLIENT TASK-MANAGER **");
        initPID();
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

}
