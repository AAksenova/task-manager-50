package ru.t1.aksenova.tm.command.task;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.aksenova.tm.api.endpoint.ITaskEndpoint;
import ru.t1.aksenova.tm.command.AbstractCommand;
import ru.t1.aksenova.tm.dto.model.TaskDTO;
import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.enumerated.Status;

import java.util.List;

@Getter
@Setter
@Component
public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    @Autowired
    protected ITaskEndpoint taskEndpoint;

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void showTask(final TaskDTO task) {
        if (task == null) return;
        System.out.println("TASK ID: " + task.getId());
        System.out.println("TASK NAME: " + task.getName());
        System.out.println("TASK DESCRIPTION: " + task.getDescription());
        System.out.println("TASK STATUS: " + Status.toName(task.getStatus()));
        System.out.println("TASK PROJECT ID: " + task.getProjectId());
    }

    protected void renderTasks(@Nullable final List<TaskDTO> tasks) {
        int index = 1;
        for (final TaskDTO task : tasks) {
            if (task == null) continue;
            System.out.println(index);
            showTask(task);
            index++;
            System.out.println("---------------------");
        }
    }

}
