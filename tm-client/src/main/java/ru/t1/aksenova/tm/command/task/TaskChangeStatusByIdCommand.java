package ru.t1.aksenova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.aksenova.tm.dto.request.TaskChangeStatusByIdRequest;
import ru.t1.aksenova.tm.enumerated.Status;
import ru.t1.aksenova.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public final class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-change-status-by-id";

    @NotNull
    public static final String DESCRIPTION = "Change task status by id.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS BY ID]");
        System.out.println("ENTER TASK ID:");
        @NotNull final String id = TerminalUtil.nextLine();

        System.out.println("ENTER PROJECT STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();

        @Nullable final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(getToken());
        request.setId(id);
        request.setStatus(statusValue);
        getTaskEndpoint().changeTaskStatusById(request);
    }

}
