package ru.t1.aksenova.tm.command.project;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.aksenova.tm.api.endpoint.IProjectEndpoint;
import ru.t1.aksenova.tm.command.AbstractCommand;
import ru.t1.aksenova.tm.dto.model.ProjectDTO;
import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.enumerated.Status;

@Getter
@Setter
@Component
public abstract class AbstractProjectCommand extends AbstractCommand {

    @Nullable
    @Autowired
    protected IProjectEndpoint projectEndpoint;

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void showProject(@Nullable final ProjectDTO project) {
        if (project == null) return;
        System.out.println("PROJECT ID: " + project.getId());
        System.out.println("PROJECT NAME: " + project.getName());
        System.out.println("PROJECT DESCRIPTION: " + project.getDescription());
        System.out.println("PROJECT STATUS: " + Status.toName(project.getStatus()));
    }

}
